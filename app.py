import urllib3
from flask import Flask
from flask_restful import Api

from flask_cors import CORS

from connections.connect import Connect,FilterAllCommitsUser

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

app = Flask(__name__)
cors = CORS(app)

api = Api(app)
api.add_resource(Connect,'/connect/','/connect/<int:_id>',endpoint='connect')
api.add_resource(FilterAllCommitsUser,'/filter-user/', endpoint='filter-user')


if __name__ == "__main__":
    app.run(debug=False,host='0.0.0.0',port=9001)