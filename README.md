Instruçoes Download p/ DataBase e Filter Dados

-- Pre Requisitos:
    - Executar primeiro projeto robot-GitLab
        * ver README.md do projeto
    - Ter instalado:
        * Makefile
        * Pyhton3
        * Docker
        * Docker-compose

1 - Execute:
        make connect

2 - Verifique se o ip do Serviço esta correto:
        docker ps
        docker inspect "ID Container" | grep IP

        ip_service = 172.19.0.2

3- Baixar dados e salvar:
    
    http://172.18.0.2:9001/connect/
    local = http://127.0.0.1:9001/connect/


4- Filtrar user com mais commits / qual o ultimo commit do usuario /quando foi o commit do usuário:
    
    http://172.18.0.2:9001/filter-user/
    local = http://127.0.0.1:9001/filter-user/