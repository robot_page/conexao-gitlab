from flask_restful import Resource, ResponseBase,abort
import json

import requests
from bs4 import BeautifulSoup

from flask import jsonify, make_response
import operator

from connections.exception_errors import Page_NotFound

from constants import URL_SERVICE_DB_USERS,URL_SERVICE_DB_COMMITS,URL_SERVICE_DB_COMMITS_USER,URL_SERVICE_DB_COMMITS_LAST,\
    URL_GITLAB_LOGIN,URL_GITLAB_COMMITS,URL_GITLAB_REQUESTS,URL_SERVICE_DB_DROP_TABLE

class Connect(Resource):
    def get(self):

        self.headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
        }

        login_data = {
        'commit': 'Sign in',
        'utf8': '%E2%9C%93',
        'user[login]': "testdrl-gitlab",
        'user[password]': "tstdrlgitlab"
        }

        self.session = requests.Session()
        response = self.session.get(URL_GITLAB_LOGIN, headers=self.headers, verify=False)
        
        soup = BeautifulSoup(response.text, 'html.parser')
        login_data['authenticity_token'] = soup.find(
        'input', attrs={'name': 'authenticity_token'})['value']

        response = self.session.post(URL_GITLAB_LOGIN, data=login_data, headers=self.headers, verify=False)
        
        check_status = self.session.get(URL_GITLAB_REQUESTS, headers=self.headers, verify=False)

        self.status_page = check_status

        ########## CASE PAGE NOT FOUND
        message = None
        try:
            soup_check = BeautifulSoup(check_status.text, 'html.parser')
            check_acesso = soup_check.find('div', attrs={"class":"container"}).find('h3')
            for check in check_acesso:
                if check != []:
                    raise Page_NotFound
        except AttributeError as e:
            pass
        except Page_NotFound as e:
            message = "algo deu errado, verifique sua conexao, login GitLab. " + str(e)
        finally:
            if message:
                message = message
                retcode = 400
                return message,retcode
            else:
                message = None
                retcode = 200
                print('passo aqui')

                self.downloads_dados()


    def downloads_dados(self):
            ########## NUMERO DE COMMITS
            lst_nb = []
            nb_commit = BeautifulSoup(self.status_page.text, 'html.parser')
            nb_cmt = nb_commit.find_all('strong', attrs={"class":"project-stat-value"})
            for nb in nb_cmt:
                lst_nb.append(nb)
                    
            commit = str(lst_nb[1].text)
            new_commit = commit.replace(',','')

            calc = int(new_commit) / 100
            if not calc == int:
                new_calc = str(calc)
                page = new_calc.split('.')
                loop_page = int(page[0]) + 1

            #loop_page = 3
            page = 0
            
            self.offset = 0

            list_page_user = []
            list_page_date = []
            list_page_hash = []

            list_full = []
            list_user_id = []

            while page < loop_page:
                ######### CLEAR TABLES
                if page == 0:
                    drop_table = requests.delete(URL_SERVICE_DB_DROP_TABLE)

                ######### BUSCAR USER, DATE, COMMITS
                response = self.session.get(URL_GITLAB_COMMITS.format(self.offset), headers=self.headers, verify=False)
                soup_rows = BeautifulSoup(response.text, 'html.parser')

                ########## USER
                rws_user = soup_rows.find_all("a", attrs={"class":"commit-author-link"})
                for r_usr in rws_user:
                    userNotSapce = r_usr.text.replace(' ','-')
                    list_page_user.append(userNotSapce)
                self.list_all_user = list_page_user
                
                ######### DATE
                rws_date = soup_rows.find_all('time', attrs={"class":"js-timeago"})
                for d in rws_date:
                    list_page_date.append(d['datetime'])

                ######## HASH
                rws_hash = soup_rows.find_all('a', attrs={"class":"commit-row-message item-title js-onboarding-commit-item"}, href=True)
                for h in rws_hash:
                    list_page_hash.append(h['href'])


                self.save_user_not_duplicate()
                

                '''if self.save_userDB.status_code == 200:
                    for usr in list_page_user:
                        userDB = requests.get(URL_SERVICE_DB_USERS+usr).json()
                        list_user_id.append(userDB['id_user'])

                    ############# SAVE COMMITS IN COMMITSDB
                    for n in range(len(list_page_user)):
                        obj_commit = {
                            "date":list_page_date[n],
                            "hashcommit":list_page_hash[n],
                            "user_id":list_user_id[n]
                        }

                        print(n,obj_commit)
                        
                        save_commitDB = requests.post(URL_SERVICE_DB_COMMITS,obj_commit)
                    

                    cont = self.offset + 100
                    self.offset = cont

                    cpage = page + 1
                    page = cpage

                    list_page_user.clear()
                    list_page_date.clear()
                    list_page_hash.clear()
                    list_user_id.clear()

                    print(page,self.offset)

                    if page == loop_page:
                        msg = {"message":"Download Concluido com Sucesso"}
                        return make_response(jsonify(msg),201)
                        
                else:
                    abort(404,message="algo deu errado, save commitDB")

                        
            else:
                abort(404,message="algo deu errado, verifique sua conexao, login GitLab")'''
                
    def save_user_not_duplicate(self):
        ########## SAVE USER IN USERD

        list_not_duplicate = []

        for lst in self.list_all_user:
            if self.offset > 0:
                search = []
                search_user = requests.get(URL_SERVICE_DB_USERS).json()
                
                for data in search_user:
                    search.append(data['user'])
                
                if lst not in search:
                    if lst not in list_not_duplicate:
                        list_not_duplicate.append(lst)
                        search.clear()
                            
            else:
                if lst not in list_not_duplicate:
                    list_not_duplicate.append(lst)

        #print(list_not_duplicate)
        id = 'id_user'
        user = 'user'
        
        lst_dict_not_duplicate = []

        for i in list_not_duplicate:
            dict_not_duplicate = json.dumps({id:None, user:i})
            lst_dict_not_duplicate.append(dict_not_duplicate)

        print(lst_dict_not_duplicate)

        #self.save_userDB = requests.post(URL_SERVICE_DB_USERS,lst_dict_not_duplicate)

        '''for usr in list_not_duplicate:
            obj_user = {
                "user": usr
            }

            self.save_userDB = requests.post(URL_SERVICE_DB_USERS,obj_user)
        
        list_not_duplicate.clear()'''

        #print('status',save_userDB.status_code)

class FilterAllCommitsUser(Resource):
    def get(self):
        all_user = requests.get(URL_SERVICE_DB_COMMITS).json()
        list_all_user = []
        
        for user in all_user:
            list_all_user.append(user['user_id'])
            
        dict_all_user = {i:list_all_user.count(i) for i in list_all_user}
        max_commit_id_user = str(max(dict_all_user.items(), key=operator.itemgetter(1))[0])
        
        for id_usr,n_commits in dict_all_user.items():
            if id_usr == int(max_commit_id_user):
                number_commits = n_commits

        name_user = requests.get(URL_SERVICE_DB_USERS+max_commit_id_user).json()
        name_user["total_commits"] = number_commits
        
        last_commit = requests.get(URL_SERVICE_DB_COMMITS_LAST+max_commit_id_user).json()
        
        new_last_commit = {
            "id_commit" : last_commit['id_commit'],
            "date" : last_commit['date'],
            "hashcommit" : last_commit['hashcommit'],
            "user" : name_user['user']
        }


        dados = {
            "User com mais commits" : name_user,
            "Ultimo commit feito" : new_last_commit,
            "Data ultimo commit" : last_commit['date']
        }
        
        return dados,200
        
