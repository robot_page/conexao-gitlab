from os import getenv

'''URL_SERVICE_DB_USERS = getenv('URL_SERVICE_DB_USERS','http://172.18.0.3:9000/users/')
URL_SERVICE_DB_COMMITS = getenv('URL_SERVICE_DB_COMMITS','http://172.18.0.3:9000/commits/')
URL_SERVICE_DB_COMMITS_USER = getenv('URL_SERVICE_DB_COMMITS_USER','http://172.18.0.3:9000/commits-user/')
URL_SERVICE_DB_COMMITS_LAST = getenv('URL_SERVICE_DB_COMMITS_LAST','http://172.18.0.3:9000/last-commit/')
URL_SERVICE_DB_DROP_TABLE = getenv('URL_SERVICE_DB_DROP_TABLE','http://172.18.0.3:9000/drop-table/')'''


URL_SERVICE_DB_USERS = getenv('URL_SERVICE_DB_USERS','http://127.0.0.1:9000/users/')
URL_SERVICE_DB_COMMITS = getenv('URL_SERVICE_DB_COMMITS','http://127.0.0.1:9000/commits/')
URL_SERVICE_DB_COMMITS_USER = getenv('URL_SERVICE_DB_COMMITS_USER','http://127.0.0.1:9000/commits-user/')
URL_SERVICE_DB_COMMITS_LAST = getenv('URL_SERVICE_DB_COMMITS_LAST','http://127.0.0.1:9000/last-commit/')
URL_SERVICE_DB_DROP_TABLE = getenv('URL_SERVICE_DB_DROP_TABLE','http://127.0.0.1:9000/drop-table/')


'''URL_GITLAB_LOGIN = getenv('URL_GITLAB_LOGIN','https://gitlab.com/users/sign_in')
URL_GITLAB_REQUESTS = getenv('URL_GITLAB_REQUESTS','https://gitlab.com/softplan/requests')
URL_GITLAB_COMMITS = getenv('URL_GITLAB_COMMITS','https://gitlab.com/softplan/requests/commits/master?limit=100&offset={}')'''

URL_GITLAB_LOGIN = getenv('URL_GITLAB_LOGIN','https://gitlab.com/users/sign_in')
URL_GITLAB_REQUESTS = getenv('URL_GITLAB_REQUESTS','https://gitlab.com/tezos/tezos')
URL_GITLAB_COMMITS = getenv('URL_GITLAB_COMMITS','https://gitlab.com/tezos/tezos/commits/master?limit=100&offset={}')

